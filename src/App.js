import React, {Component} from 'react';
import './App.less';
import {Provider} from "react-redux";
import configStore from './store';
import Name from "./components/Name";

class App extends Component {
  render() {
    return (
      <Provider store={configStore}>
        <div className='App'>
          <Name />
        </div>
      </Provider>
    );
  }
}

export default App;