const initialState = {
  name: 'friend',
  age: 0
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_NAME':
      return {
        ...state,
        name: action.name
      };
    default:
      return state;
  }
}