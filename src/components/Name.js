import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {setName} from '../actions/nameAction'
import nameReducer from "../reducers/nameReducer";

class Name extends React.Component{
  handleInputChange(event){
    const name = event.target.value;
    this.props.setName(name);
  }

  render() {
      const {name} = this.props.nameReducer;
    return (
      <section>
        <input type={'text'} onChange={this.handleInputChange.bind(this)}/>
        <p>Hello,{name}!</p>
      </section>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  setName
}, dispatch);

const mapStateToProps = (state) => ({
  nameReducer: state.nameReducer111
}
)
export default connect(mapStateToProps, mapDispatchToProps)(Name);